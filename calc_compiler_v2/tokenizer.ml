open Str

type token = 
    | Tok_Number of int
    | Tok_Plus
    | Tok_Minus
    | Tok_Div
    | Tok_Mul
    | Open_Parenthesis
    | Close_Parenthesis
    | Whitespace
    | Error
    | EOF

exception Invalid_Input

let reg_number = Str.regexp "[0-9]+"
let reg_plus   = Str.regexp "[+]"
let reg_minus  = Str.regexp "[-]"
let reg_div    = Str.regexp "/"
let reg_mul    = Str.regexp "*"
let reg_open_paren  = Str.regexp "("
let reg_close_paren = Str.regexp ")"
let reg_whitespace = Str.regexp " "

let reg_list = [reg_number; 
                reg_plus; reg_minus; 
                reg_div; reg_mul; 
                reg_open_paren; reg_close_paren; 
                reg_whitespace
                ];;

let lexeme_to_token (lexeme: string) : token =
    match lexeme with
    | "+" -> Tok_Plus
    | "-" -> Tok_Minus
    | "(" -> Open_Parenthesis
    | ")" -> Close_Parenthesis
    | "/" -> Tok_Div
    | "*" -> Tok_Mul
    | _   -> if Str.string_match reg_number lexeme 0 
                then Tok_Number(int_of_string lexeme)
             else if Str.string_match reg_whitespace lexeme 0 
                then Whitespace
             else Error

let rec acquire_token (input: string) (regs: regexp list) (pos: int) : token * int =
    match regs with
    | [] -> (Error, pos)
    | r :: rs -> if Str.string_match r input pos then
                    let lexeme = Str.matched_string input in
                    let new_pos = pos + String.length lexeme in
                    (lexeme_to_token lexeme, new_pos)
                else acquire_token input rs pos

let tokenize_v2 (input: string) : token list =
    let rec tokenize' (input: string) (reg_exp: regexp list) (pos: int) (accm: token list) =
        if pos >= String.length input then (accm @ [EOF] ) 
        else
        (match acquire_token input reg_exp pos with
        | (Error, _)                  -> tokenize' input reg_exp (pos+1) (accm @ [Error])
        | (Whitespace, new_position)  -> tokenize' input reg_exp new_position accm
        | (tok, new_position) -> tokenize' input reg_exp new_position (accm @ [tok]) )
    in
    tokenize' input reg_list 0 []
;;

let tokenize (input: string) : token list =
    let rec tokenize' (input: string) (pos: int) (accm: token list) : token list=
        if Str.string_match reg_number input pos then 
            let number_string = Str.matched_string input in
            let offset = pos + String.length number_string in
            tokenize' input offset (accm @ [Tok_Number(int_of_string number_string)])
        else if Str.string_match reg_plus input pos then
            let offset = pos + 1 in
            tokenize' input offset (accm @[Tok_Plus])
        else if Str.string_match reg_minus input pos then
            let offset = pos + 1 in
            tokenize' input offset (accm @ [Tok_Minus])
        else if Str.string_match reg_whitespace input pos then
            let spaces = Str.matched_string input in
            let offset = pos + String.length spaces in
            tokenize' input offset (accm)
        else accm @ [ EOF ]
    in
    tokenize' input 0 []
