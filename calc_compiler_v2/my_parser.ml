open Printf
exception IllegalExpression of string

let look_ahead (toks: token list) : token =
    match toks with
    | []      -> raise (IllegalExpression "No more tokens to look at")
    | h :: tl -> h
;;

let match_tok a (toks: token list)=
    match toks with
    | []      -> raise (IllegalExpression "No more tokens to match")
    | h :: tl -> if a = h then tl
                 else raise (IllegalExpression "Tokens are not equal")
;;

(*
 * E -> T + E | T | T - E
 * T -> P * T | P / T | P
 * P -> 1|2|3|4|...
 * Op_first -> + | -
 *)

type expr = 
    | Sum of expr * expr
    | Minus of expr * expr
    | Mul of expr * expr
    | Div of expr * expr
    | Integer of int

let rec parse_E (toks: token list) : expr * token list =
    let (a1, toks) = parse_T toks in
    let t = look_ahead toks in
    match t with
    | Tok_Plus  -> let (a2, toks) = parse_E (match_tok Tok_Plus toks) 
               in
               (Sum(a1,a2), toks)
    | Tok_Minus -> let (a2, toks) = parse_E (match_tok Tok_Minus toks) 
               in
               (Minus(a1,a2), toks)
    | _         -> (a1, toks)
and parse_T (toks: token list) : expr * token list =
    let (a1, toks) = parse_P toks in
    let t = look_ahead toks in
    match t with
    | Tok_Mul -> let (a2, toks) = parse_T (match_tok Tok_Mul toks)
                 in
                 (Mul(a1, a2), toks)
    | Tok_Div -> let (a2, toks) = parse_T (match_tok Tok_Div toks)
                 in
                 (Div(a1, a2), toks)
    | _       -> (a1, toks)
and parse_P (toks: token list) : expr * token list =
    let t = look_ahead toks in
    match t with
    | Tok_Number x -> (Integer x, (match_tok (Tok_Number x) toks))
    | _            -> raise (IllegalExpression "Invalid terminal at T")

