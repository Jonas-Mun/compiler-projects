open Str

type token =
    | Tok_Num of string
    | Tok_Plus
    | Tok_End
    | Error
;;

let reg_number = Str.regexp "[0-9]+"
let reg_plus = Str.regexp "[+]"


(* Scalable Tokenizer *)
let make_token lexeme = 
    match lexeme with
    | "+" -> Tok_Plus
    | _ -> Tok_Num lexeme
;;

let rec specify_token reg_l str pos =
    match reg_l with
    | r :: rs when Str.string_match r str pos ->
            let lexeme = Str.matched_string str
            in
            (String.length lexeme, make_token lexeme)
    | r :: rs -> specify_token rs str pos
    | [] -> (0, Error)
;;

let new_lex str =
    let reg_list = [reg_number; reg_plus] in
    let rec lex' reg_list pos str accm =
        if pos >= String.length str then
            accm @ [Tok_End]
        else 
            let (lex_len, tok) = specify_token reg_list str pos in
            let new_pos = pos + lex_len
            in
            match tok with
            | Error -> accm @ [Error]
            | _ -> lex' reg_list new_pos str (accm @ tok)
    in
    lex' reg_list 0 str []
;;



(* Manual Tokenizer *)
let lex str =
    let rec lex' pos str accm =
        if pos >= String.length str then
            accm @ [Tok_End]
        (* Number check *)
        else if Str.string_match reg_number str pos then
            let lexeme = Str.matched_string str in
            let new_pos = pos + (String.length lexeme) 
            in 
            lex' new_pos str (accm @ [(Tok_Num lexeme)])
        else if Str.string_match reg_plus str pos then
            let lexeme = Str.matched_string str in
            let new_pos = pos + (String.length lexeme)
            in
            lex' new_pos str (accm @ [Tok_Plus])
        else accm @ [Error]
    in
    lex' 0 str []
;;
