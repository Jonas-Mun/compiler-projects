(*open Str*)
(*#load "str.cma" *)

type token =
    | Identifier
    | Keyword
    | Number
    | Err

let reg_string = Str.regexp "[a-zA-Z]"
let reg_num    = Str.regexp "[0-9]"

let rec token_string (s: string) (p: int) : token * int =
    if Str.string_match reg_string s p then 
        token_string s (p+1)
    else if Str.string_match reg_num s p then
        (Err,p)
    else 
        (Identifier, p-1)
;;

let rec token_num (n: string) (p: int) : token * int=
    if Str.string_match reg_num n p then
        token_num n (p+1)
    else if Str.string_match reg_num n p then
        (Err, p)
    else 
        (Number, p-1)
;;

(* Have tokenizer select reg ex from this list *)
let f_reg = [(reg_string, token_string); (reg_num, token_num)]

let tokenize (s: string) : token list =
    let rec tokenize' s p accm =
        if p < String.length s then
            if s.[p] = ' ' then tokenize' s (p+1) accm 
            else
                let (_,reg_f) = List.find (fun reg -> Str.string_match (fst reg) s p) f_reg in
                let tok_pos = reg_f s p
                in
                match tok_pos with
                | (Err, _) -> []
                | (tok, pos) -> tokenize' s (pos+1) (accm @ [tok])
            else 
                accm 
                in
    tokenize' s 0 []
;;

