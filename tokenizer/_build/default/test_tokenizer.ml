open OUnit2
open Tokenizer

let test_string_token _ = assert_equal (Identifier, 1) (Tokenizer.token_string "in" 0)
let test_int_token    _ = assert_equal (Number, 0) (Tokenizer.token_num "1" 0)

let test_tokenize _ = assert_equal [Identifier; Number] (tokenize "in 2")
let test_tokenize_one _ = assert_equal [Number; Number; Identifier] (tokenize "23 54 wasd")

let suite = "suit">:::
    ["test_string_token">:: test_string_token;
     "test_int_token"   >:: test_int_token;
     "test_tokenize"    >:: test_tokenize;
     "test_tokenize_one">:: test_tokenize_one]
;;

let () =
    run_test_tt_main suite
;;

