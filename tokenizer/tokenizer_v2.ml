type token = 
  | Keyword of string
  | Identifier of string
  | Number of int
  | Err

let keywords = ["char"; "int"; "if"; "else"]

(* Concatenate strings in a list with a delim *)
let flatten (l: string list) (delim: string) : string =
    let rec aux l delim accm =
        match l with 
        | [] -> accm
        | [x] -> accm ^ x
        | x :: xs -> aux xs delim (accm ^ x ^ delim)
    in
    aux l delim ""
;;

(* Regular Expressions *)
let reg_keywords ks = 
    let reg = flatten k "\\|" in
    Str.regex reg
;;

let reg_identifier = Str.regex "[a-zA-Z]";;

let reg_number = Str.regex "[0-9]";;

(* *)
let make_token (s: string) : token =
    if Str.match_string reg_keywords s 0 
    then Keyword s
    else if Str.match_string reg_identifier s 0 
    then Identifier s
    else if Str.match_string reg_number s 0
    then Number int_of_string(s)
    else Err
;;


let tokenize (sl: string list) : token list =
    let rec aux l accm =
        match l with
        | [] -> accm
        | x :: xs -> aux xs (accm @ [make_token x])
    in
    aux sl []

